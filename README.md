Repo for thesis Intersection Attacks on Anonymous Microblogging

## Run Tests On Sample Dataset
***
```
$ cd code
$ gradle build
$ gradlew runTestClass
```