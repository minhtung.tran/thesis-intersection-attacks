package parser;

import analyzer.models.Hashtag;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Trial {
    static long total = 22524846;
    static long i = 0;

    public static void main(String[] args) throws InterruptedException {
        String a = "\u0431\u043b\u043e\u0433\u0438\u0441\u0438\u0442\u0438\u0441\u0430\u0445\u0430";
        System.out.println(new String(a.getBytes(StandardCharsets.UTF_8)));
    }

    static void run(List<Integer> sortedList, int returnNum) throws InterruptedException {

        List<Integer> returnList = new ArrayList<>();
        int maxVal = sortedList.get(0);
        for (Integer entry : sortedList) {
            if (entry.equals(maxVal)) {
                returnList.add(entry);
            } else {
                if (returnList.size() < returnNum) {
                    maxVal = entry;
                    returnList.add(entry);
                } else
                    break;
            }
        }

        for(Integer i : returnList) {
            System.out.print(i + " ");
        }
    }

    class WorkerThread implements Runnable {

        public static long taskDone = 0;
        private final static Object lock = new Object();

        public WorkerThread(){
        }
        @Override
        public void run() {
            writeUserData();
        }

        void writeUserData() {
            String wPath = "C:\\Users\\Admin\\OneDrive\\Desktop\\Skripts\\Thesis\\Repo\\local\\";
            try {
                File file = new File(wPath + "\\"+"file.txt");
                synchronized (lock) {
                    System.out.println(file.exists());
                    boolean exists = file.exists();
                    if (!exists) {
                        file.createNewFile();
                    }
                    FileWriter fw = new FileWriter(file, true);
                    BufferedWriter bw = new BufferedWriter(fw);
                    if (exists) bw.newLine();
                    bw.write(Thread.currentThread().getName() + " has written");
                    bw.close();
                    System.out.println("done " + taskDone);
                    incrementCount();
                    checkProgress();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public static synchronized void incrementCount() {
            taskDone++;
        }

        public synchronized void checkProgress() {
            if(i > WorkerThread.taskDone)
                this.notify();
        }
    }
}
