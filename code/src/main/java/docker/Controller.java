package docker;

import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.model.*;
import com.github.dockerjava.core.DockerClientBuilder;

import java.util.List;
@SuppressWarnings( "deprecation" )
public class Controller {
    private DockerClient dockerClient;
    private String networkID = "";
    private String bridgeNetwork = "";
    private Network network;
    private final String dataPath = "//c/Users/Admin/Desktop/Skripts/Thesis/Repo/local/vm-mount";

    public static void main(String[] args) {
        Controller c = new Controller();

        c.createTCPServerContainer();
        c.createProxyServerContainer();
        c.createClientContainer();
    }

    public Controller() {
        dockerClient = DockerClientBuilder
                .getInstance("tcp://127.0.0.1:2375")
                .build();

        List<Image> images = dockerClient.listImagesCmd().exec();
        for(Image i : images) {
            System.out.println(i.getId());
        }
        List<Network> nets = dockerClient.listNetworksCmd().exec();
        for(Network n : nets) {
            System.out.println(n.getName()+ " " + n.getId());
            if (n.getName().equals("bridge"))
                bridgeNetwork = n.getId();
            if (n.getName().equals("thesis-net")) {
                networkID = n.getId();
                network = n;
            }
        }
    }

    private void createTCPServerContainer(){
        ContainerNetwork config = new ContainerNetwork().
                withIpv4Address("172.18.0.3").
                withNetworkID(networkID);
        Volume volume = new Volume(dataPath);
        CreateContainerResponse container
                = dockerClient.createContainerCmd("tcpserver:latest")
                .withHostName("tcpserver.com")
                .withName("tcp_server")
                .withVolumes(volume)
                .withBinds(Bind.parse(dataPath+":/data/"))
                .exec();
        dockerClient.connectToNetworkCmd()
                .withContainerId(container.getId())
                .withNetworkId(networkID)
                .withContainerNetwork(config)
                .exec();
        dockerClient.disconnectFromNetworkCmd()
                .withContainerId(container.getId())
                .withNetworkId(bridgeNetwork)
                .exec();
        dockerClient.startContainerCmd(container.getId())
                .exec();
    }

    private void createProxyServerContainer(){
        ContainerNetwork config = new ContainerNetwork().
                withIpv4Address("172.18.0.2").
                withNetworkID(networkID);
        CreateContainerResponse container
                = dockerClient.createContainerCmd("proxyserver:latest")
                .withName("proxy_server")
                .withHostName("proxyserver.com").exec();
        dockerClient.connectToNetworkCmd()
                .withContainerId(container.getId())
                .withNetworkId(networkID)
                .withContainerNetwork(config)
                .exec();
        dockerClient.disconnectFromNetworkCmd()
                .withContainerId(container.getId())
                .withNetworkId(bridgeNetwork)
                .exec();
        dockerClient.startContainerCmd(container.getId()).exec();
    }

    private void createClientContainer() {
        ContainerNetwork config = new ContainerNetwork();
        config.withIpv4Address("172.18.0.3").withNetworkID(networkID);

        Volume volume = new Volume(dataPath);
        CreateContainerResponse container
                = dockerClient.createContainerCmd("socketclient:latest")
                .withName("client")
                .withVolumes(volume)
                .withBinds(Bind.parse(dataPath+":/data/"))
                .exec();
        dockerClient.connectToNetworkCmd()
                .withContainerId(container.getId())
                .withNetworkId(networkID)
                .withContainerNetwork(config)
                .exec();
        dockerClient.disconnectFromNetworkCmd()
                .withContainerId(container.getId())
                .withNetworkId(bridgeNetwork)
                .exec();
        dockerClient.startContainerCmd(container.getId()).exec();
    }
}
