package analyzer.models;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class Round {
    private final int no;
    private Map<Hashtag, Integer> hashtagMap;
    private Map<User, Integer> userMap;

    public Round(int no) {
        this.no = no;
        hashtagMap = new Hashtable<>();
        userMap = new Hashtable<>();
    }

    public int getNo() {
        return no;
    }

    public void addHashtag(Hashtag hashtag) {
        Integer value = this.hashtagMap.get(hashtag);
        if (value != null)
            this.hashtagMap.replace(hashtag, value + 1);
        else
            this.hashtagMap.put(hashtag, 1);
    }

    public void addUser(User user) {
        Integer value = this.userMap.get(user);
        if (value != null)
            this.userMap.replace(user, value + 1);
        else
            this.userMap.put(user, 1);
    }

    public Map<Hashtag, Integer> getHashtags() {
        return hashtagMap;
    }

    public Map<User, Integer> getUserMap() {
        return this.userMap;
    }
}
