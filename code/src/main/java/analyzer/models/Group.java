package analyzer.models;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

public class Group {
    private int id;
    private List<Client> clients = new ArrayList<>();
    private final Map<Round, Integer> roundMap;

    public Group(int id) {
        this.id = id;
        roundMap = new Hashtable<>();
    }

    public void addClient(Client c) {
        if(!clients.contains(c)) {
            this.clients.add(c);
            c.setGroup(this);
        }
        c.getRounds().forEach((k, v) -> {
            Integer value = roundMap.get(k);
            if(value == null)
                roundMap.put(k, v);
            else {
                if(v > value)
                    roundMap.replace(k, v);
            }
        });
    }

    public List<Client> clients(){
        return clients;
    }

    public int getId() {
        return id;
    }

    public Map<Round, Integer> getRounds() {
        return roundMap;
    }
}
