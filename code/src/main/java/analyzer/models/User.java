package analyzer.models;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class User {
    private final String id;
    private Map<Hashtag, Integer> hashtagMap;
    private Map<Round, Integer> roundMap;

    public User(String id) {
        this.id = id;
        hashtagMap = new Hashtable<>();
        roundMap = new Hashtable<>();
    }

    public String getId() {
        return this.id;
    }

    public Map<Hashtag, Integer> getHashtags() {
        return this.hashtagMap;
    }

    public void addHashtag(Hashtag hashtag) {
        Integer value = this.hashtagMap.get(hashtag);
        if (value != null)
            this.hashtagMap.replace(hashtag, value + 1);
        else
            this.hashtagMap.put(hashtag, 1);
    }

    public void addRound(Round round) {
        Integer value = this.roundMap.get(round);
        if (value != null)
            this.roundMap.replace(round, value + 1);
        else
            this.roundMap.put(round, 1);
    }

    public int totalPosts() {
        AtomicInteger n = new AtomicInteger();
        roundMap.forEach((k, v) -> n.set(n.get() + v));
        return n.get();
    }

    public Map<Round, Integer> getRounds() {
        return roundMap;
    }

    public String hashtagsToString() {
        AtomicReference<String> s = new AtomicReference<>("");
        hashtagMap.forEach((k, v) -> s.set(s + k.getName() + " "));
        return s.get().trim();
    }
}
