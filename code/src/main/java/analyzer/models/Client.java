package analyzer.models;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class Client {
    private String id;
    private Map<Round, Integer> roundMap;
    private Group group;

    public Client(String id) {
        this.id = id;
        roundMap = new Hashtable<>();
    }

    public String getId() {
        return this.id;
    }

    public int getTotalPosts() {
        AtomicInteger result = new AtomicInteger();
        roundMap.forEach((round, integer) -> result.addAndGet(integer));
        return result.get();
    }

    public Map<Round, Integer> getRounds() {
        return this.roundMap;
    }

    public void addRound(Round round) {
        Integer value = this.roundMap.get(round);
        if (value != null)
            this.roundMap.replace(round, value + 1);
        else
            this.roundMap.put(round, 1);
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Group getGroup() {
        return this.group;
    }

    public String getRoundtoString() {
        AtomicReference<String> s = new AtomicReference<>("");
        roundMap.forEach((k, v) -> s.set(s.get() + k.getNo() + " "));

        return s.get().trim();
    }

    public String postingTraces() {
        String s = "";
        List<Round> list = new ArrayList<>(roundMap.keySet());
        list.sort(Comparator.comparingInt(Round::getNo));
        for(Round r : list) {
            s += r.getNo() + ":" + roundMap.get(r) + " ";
        }
        return s.trim();
    }
}
