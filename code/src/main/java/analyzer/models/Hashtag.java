package analyzer.models;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Hashtag {
    private final String name;
    private Map<Round, Integer> roundMap;
    private Map<User, Integer> userMap;

    public Hashtag(String name) {
        this.name = name;
        this.roundMap = new Hashtable<>();
        this.userMap = new Hashtable<>();
    }

    public String getName() {
        return name;
    }

    public Map<Round, Integer> getRoundMap() {
        return roundMap;
    }

    public Map<User, Integer> getUserMap() {
        return userMap;
    }

    public int getTotalPosts() {
        AtomicInteger n = new AtomicInteger();
        this.roundMap.forEach((k, v) -> n.addAndGet(v));
        return n.get();
    }

    public void addRound(Round round) {
        Integer value = this.roundMap.get(round);
        if (value != null)
            this.roundMap.replace(round, value + 1);
        else
            this.roundMap.put(round, 1);
    }

    public void addUser(User user) {
        Integer value = this.userMap.get(user);
        if (value != null)
            this.userMap.replace(user, value + 1);
        else
            this.userMap.put(user, 1);
    }

    public String hashtagTraces() {
        String s = "";
        List<Round> list = new ArrayList<>(roundMap.keySet());
        list.sort(Comparator.comparingInt(Round::getNo));
        for(Round r : list) {
            s += r.getNo() + ":" + roundMap.get(r) + " ";
        }
        return s.trim();
    }
}
