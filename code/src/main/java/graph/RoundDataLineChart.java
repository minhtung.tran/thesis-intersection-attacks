package graph;
import javax.swing.JFrame;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RoundDataLineChart extends JFrame{
    private static final long serialVersionUID = 1L;
    private static final String path = "C:\\Users\\Admin\\Desktop\\hiwi tud\\msgs_each_round.txt";

    public RoundDataLineChart(String applicationTitle, String chartTitle) throws IOException {
        super(applicationTitle);

        // based on the dataset we create the chart
        JFreeChart pieChart = ChartFactory.createXYLineChart(chartTitle, "Round", "Number of messages", createDataset(),PlotOrientation.VERTICAL, true, true, false);

        // Adding chart into a chart panel
        ChartPanel chartPanel = new ChartPanel(pieChart);

        // settind default size
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));

        // add to contentPane
        setContentPane(chartPanel);
    }

    private XYDataset createDataset() throws IOException {
        final XYSeries length1 = new XYSeries("1800");
        final XYSeries length2 = new XYSeries("3600");
        final XYSeries length3 = new XYSeries("7200");
        List<XYSeries> lst = new ArrayList<>();
        lst.add(length1);
        lst.add(length2);
        lst.add(length3);
        FileReader fr = new FileReader(path);
        BufferedReader br= new BufferedReader(fr);
        String line;
        int lineCounter = 0;
        while((line = br.readLine()) != null) {
            String[] values = line.split(" ");
            for(int i = 0; i < values.length; i++) {
                lst.get(lineCounter).add(i + 1, Double.parseDouble(values[i]));
            }
            lineCounter++;
        }
        br.close();

        final XYSeriesCollection dataset = new XYSeriesCollection();
        for(XYSeries xys : lst)
            dataset.addSeries(xys);

        return dataset;
    }

    public static void main(String[] args) throws IOException {
        RoundDataLineChart chart = new RoundDataLineChart("Dataset statistics", "Round Traffic");
        chart.pack();
        chart.setVisible(true);
    }
}
